=head1 NAME

jsvnversion - Produce a compact version number for a working copy.

=head1 SYNOPSIS

jsvnversion [options] [wc_path [trail_url]]

=head1 OVERVIEW

SVNKit is a pure Java(TM) version of Subversion.

Subversion is a version control system, which allows you to keep old
versions of files and directories (usually source code), keep a
log of who, when, and why changes occurred, etc., like CVS, RCS or
SCCS.  Subversion keeps a single  copy  of  the  master  sources.
This copy is called the source ``repository''; it contains all the
information to permit extracting previous versions of those files
at any time.

For additional information, see http://svnkit.com/.

For information about Subversion in a broad sense, can be found
at http://svnbook.red-bean.com/ and http://subversion.tigris.org.

Run `jsvnversion --help' to access the built-in tool documentation.
